# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields, ModelSingleton, Workflow
from trytond.pool import Pool
from trytond.pyson import Eval
from imaplib import IMAP4_SSL, IMAP4
import imaplib
import email
import logging
import os


_logger = logging.getLogger(__name__)


class EmailAttachment(ModelSQL, ModelView):
    """Model to store email attachments configuration"""
    __name__ = 'email.attachment'
    __order__ = 'state'

    email = fields.Char('Email', required=True, help='Email address (info@electrans.es)')
    password = fields.Char('Password', required=True, help='Email password')
    alias = fields.Char('Alias',  help='Email alias (alias@electrans.es)')
    server = fields.Char('Server', required=True, help='IMAP server (imap.gmail.com)')
    port = fields.Integer('Port', required=True, help='IMAP port (993)')
    ssl = fields.Boolean('SSL', help='Use SSL')
    file_extension = fields.Char('File extension', required=True,
                                 help='File extension separeted by comma. Example: pdf,xls,xlsx')
    path = fields.Char('Path', required=True, help='Path to save files')
    state = fields.Selection([
            ('disable', 'Disable'),
            ('enable', 'Enable'),
            ], 'State', required=True, readonly=True,)

    @classmethod
    def __setup__(cls):
        super(EmailAttachment, cls).__setup__()
        cls._buttons.update({
                'get_email_test': {},
                'disable': {
                    'invisible': Eval('state') == 'disable',
                    'depends': ['state'],
                    },
                'enable': {
                    'invisible': Eval('state') == 'enable',
                    'depends': ['state'],
                    },
                })

    @staticmethod
    def default_ssl():
        return True

    @staticmethod
    def default_state():
        return 'disable'

    @classmethod
    @ModelView.button
    def get_email_test(cls, records):
        """
        Method to check credentials against IMAP server
        :return:
        """

        for record in records:
            if record.ssl:
                server = IMAP4_SSL(record.server, record.port)
            else:
                server = IMAP4(record.server, record.port)
            try:
                server.login(record.email, record.password)
                server.logout()

            except Exception as e:
                raise UserError(gettext('electrans_email_attachment.msg_email_test_ko',
                                        email=record.email,
                                        error=e))
            raise UserError(gettext('electrans_email_attachment.msg_email_test_ok',
                                    email=record.email))

    @classmethod
    @ModelView.button
    def disable(cls, records):
        cls.write(records, {
            'state': 'disable',
        })

    @classmethod
    @ModelView.button
    def enable(cls, records):
        cls.write(records, {
            'state': 'enable',
        })

    @classmethod
    def get_email_attachment(cls):
        """
        Get email attachment method to execute as cron job
        """
        EmailAttachment = Pool().get('email.attachment')
        accounts = EmailAttachment.search([
            ('state', '=', 'enable'),
            ])
        for acc in accounts:
            try:
                # Check if contains all fields
                if not acc.email or not acc.password or not acc.server or not acc.port or not acc.file_extension or not acc.path:
                    _logger.error('Email %s not configured correctly' % acc.email)
                    break
                # Check if attachment_path not exists and create it
                if not os.path.exists(acc.path):
                    os.makedirs(acc.path)
                # Counts for logger
                email_count = 0
                file_count = 0
                # Connect to user_email account
                mail = imaplib.IMAP4_SSL(acc.server, acc.port)
                mail.login(acc.email, acc.password)
                mail.select('inbox')
                # Check if mail have alias
                if acc.alias:
                    email_to = acc.alias
                else:
                    email_to = acc.email
                # Note: When we download, the email is automatically marked it as SEEN
                # Get emails to email_to and UNSEEN
                result, data = mail.uid('search', None, '(TO "%s" UNSEEN)' % email_to)
                uids = data[0].split()
                for uid in uids:
                    result, data = mail.uid('fetch', uid, '(RFC822)')
                    raw_email = data[0][1]
                    email_message = email.message_from_bytes(raw_email)
                    email_count += 1
                    # Download attachment files if extension file is in _ALLOWED_FORMAT into attachment_path
                    for part in email_message.walk():
                        if part.get_content_maintype() == 'multipart':
                            continue
                        if part.get('Content-Disposition') is None:
                            continue
                        filename = part.get_filename()
                        if filename:
                            file_extension = os.path.splitext(filename)[1][1:].lower()
                            if file_extension in acc.file_extension.split(','):
                                file_path = os.path.join(acc.path, filename)
                                # Check if file exists and if it is not a duplicate change name and download
                                duplicated_count = 0
                                while os.path.exists(file_path):
                                    file_path = os.path.join(acc.path, '{}_{}'.format(filename, duplicated_count))
                                    duplicated_count += 1
                                # Download file
                                fp = open(file_path, 'wb')
                                fp.write(part.get_payload(decode=True))
                                fp.close()
                                file_count += 1
                mail.close()
                mail.logout()
                _logger.info('Downloaded %s files from %s emails' % (file_count, email_count))
            except Exception as e:
                _logger.error('Error downloading files from email %s' % e)
                pass
