# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import cron
from . import email_attachment


def register():
    Pool.register(
        cron.Cron,
        email_attachment.EmailAttachment,
        module='electrans_email_attachment', type_='model')
