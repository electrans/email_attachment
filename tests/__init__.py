# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

try:
    from trytond.modules.electrans_work_project.tests.test_email_attachment import suite
except ImportError:
    from .test_email_attachment import suite

__all__ = ['suite']
